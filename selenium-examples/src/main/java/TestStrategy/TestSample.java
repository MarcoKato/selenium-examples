package TestStrategy;

import org.openqa.selenium.json.JsonOutput;
import org.w3c.dom.ls.LSOutput;

import java.io.IOException;
import java.util.ArrayList;

public class TestSample {

    public static void main(String[] args) throws IOException {
        ArrayList<String> data = ExcelDataDriven.getData("add profile");

        for (int i = 0; i <data.size() ; i++) {
            System.out.println(data.get(i));
        }
    }
}
