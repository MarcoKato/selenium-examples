package TestStrategy;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelDataDriven {

     public static ArrayList<String> getData(String lookup) throws IOException {
        ArrayList<String> strAL = new ArrayList<>();
        FileInputStream fis = new FileInputStream("D:\\Documentos\\selenium\\selenium-examples\\data.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet;
        int sheetCount = wb.getNumberOfSheets();
        int k = 0;
        int column = 0;
        //look for sheet called testData
        for (int i = 0; i < sheetCount; i++) {
            if (wb.getSheetAt(i).getSheetName().equalsIgnoreCase("testData")) {
                sheet = wb.getSheetAt(i);
                Iterator<Row> rows = sheet.iterator();
                Row firstRow = rows.next();
                Iterator<Cell> cel = firstRow.cellIterator();

                while (cel.hasNext()) {
                    Cell celValue = cel.next();
                    if (celValue.getStringCellValue().equalsIgnoreCase("test cases")) {
                        column = k;
                    }
                    k++;
                }
                //
                //System.out.println(column);
                while (rows.hasNext()) {
                    Row r = rows.next();
                    if (r.getCell(column).getStringCellValue().equalsIgnoreCase(lookup)) {
                        Iterator<Cell> cellI = r.cellIterator();
                        while (cellI.hasNext()) {
                            Cell c = cellI.next();
                            if(c.getCellType()== CellType.NUMERIC){
                                strAL.add(NumberToTextConverter.toText(c.getNumericCellValue()));
                            } else if(c.getCellType() == CellType.STRING){
                                strAL.add(c.getStringCellValue());
                            }
                        }
                    }
                }
            }
        }
        return strAL;
    }
}

