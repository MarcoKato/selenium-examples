package testcases;
import objectRepository.RediffHomePage;
import objectRepository.RediffHomePagePF;
import objectRepository.RediffLogInPage;
import objectRepository.RediffLogInPagePF;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginAppPF {

    @Test
    public void Login() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
        RediffLogInPagePF rediffLogInPage = new RediffLogInPagePF(driver);
        rediffLogInPage.getUsername().sendKeys("admin");
        rediffLogInPage.getPassword().sendKeys("1234");
        rediffLogInPage.getSubmit().click();
        rediffLogInPage.getHome().click();

        RediffHomePagePF rediffHomePage = new RediffHomePagePF(driver);
        rediffHomePage.getSearch().sendKeys("lkajsdlkj");
        rediffHomePage.getSearchBtn().click();

    }
}

