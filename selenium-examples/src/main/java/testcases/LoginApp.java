package testcases;
import objectRepository.RediffHomePage;
import objectRepository.RediffLogInPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginApp {

    @Test
    public void Login() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
        RediffLogInPage rediffLogInPage = new RediffLogInPage(driver);
        rediffLogInPage.Email().sendKeys("admin");
        rediffLogInPage.Password().sendKeys("1234");
        rediffLogInPage.Submit().click();
        rediffLogInPage.Home().click();

        RediffHomePage rediffHomePage = new RediffHomePage(driver);
        rediffHomePage.Search().sendKeys("lkajsdlkj");
        rediffHomePage.SearchBtn().click();

    }
}

