package logForYeyBeta;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class logginggg {

    private static Logger log = LogManager.getLogger(logginggg.class);

    public static void main(String[] args) {
        log.debug("i have clicked on button");
        log.info("Button is displayed");
        log.error("Button is not displayed");
        log.fatal("Button is missing");
    }
}
