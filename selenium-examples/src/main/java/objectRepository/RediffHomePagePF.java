package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RediffHomePagePF {

    WebDriver driver;

    public RediffHomePagePF(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "srchword")
    WebElement search;;
    @FindBy(xpath = "//form[@id='queryTop']//input[@class='newsrchbtn']")
    WebElement searchBtn;

    public WebElement getSearch() {
        return search;
    }

    public void setSearch(WebElement search) {
        this.search = search;
    }

    public WebElement getSearchBtn() {
        return searchBtn;
    }

    public void setSearchBtn(WebElement searchBtn) {
        this.searchBtn = searchBtn;
    }
}
