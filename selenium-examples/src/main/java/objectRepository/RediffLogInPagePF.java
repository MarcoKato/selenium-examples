package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RediffLogInPagePF {

    WebDriver driver;

    public RediffLogInPagePF(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath="//input[@id='login1']")
    WebElement username;
    @FindBy(name="passwd")
    WebElement password;
    @FindBy(xpath="//input[@name='proceed']")
    WebElement submit;
    @FindBy(linkText = "Home")
    WebElement home;

    public WebElement getUsername() {
        return username;
    }

    public void setUsername(WebElement username) {
        this.username = username;
    }

    public WebElement getPassword() {
        return password;
    }

    public void setPassword(WebElement password) {
        this.password = password;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public void setSubmit(WebElement submit) {
        this.submit = submit;
    }

    public WebElement getHome() {
        return home;
    }

    public void setHome(WebElement home) {
        this.home = home;
    }

}
