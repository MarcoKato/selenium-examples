package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RediffLogInPage {

    WebDriver driver;

    public RediffLogInPage(WebDriver driver) {
        this.driver = driver;
    }

    By username = By.xpath("//input[@id='login1']");
    By password = By.name("passwd");
    By submit = By.xpath("//input[@name='proceed']");
    By home = By.linkText("Home");

    public WebElement Email(){
        return driver.findElement(username);
    }

    public WebElement Password(){
        return driver.findElement(password);
    }

    public WebElement Submit(){
        return driver.findElement(submit);
    }

    public WebElement Home(){
        return driver.findElement(home);
    }
}
