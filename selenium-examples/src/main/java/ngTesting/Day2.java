package ngTesting;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Day2 {

    @BeforeMethod
    public void bm() {
        System.out.println("im present before every method in this class");
    }

    @AfterMethod
    public void am() {
        System.out.println("im present after every method in this class");
    }

    @Test(timeOut = 4000)
    public void m1d2() {
        System.out.println("test 1 day 2");
    }

    @Test
    public void m2d2() {
        System.out.println("test 2 day 2");
    }
}
