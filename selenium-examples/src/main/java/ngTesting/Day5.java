package ngTesting;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Day5 {

    @Test(dataProvider = "dp")
    public void m1d5(String username, String password /*, String crap */){
        System.out.println(username);
        System.out.println(password);
        //System.out.println(crap);
    }

    @Test
    public void m2d5(){

    }
    @DataProvider
    public Object[][] dp(){
       Object[][] data = new Object[3][2];

        data[0][0] = "admin";
        data[0][1] = "1234";

        data[1][0] = "adminone";
        data[1][1] = "321321";

        data[2][0] = "root";
        data[2][1] = "lolz";

        /*Object[][] data = new Object[2][3];

        data[0][0] = "admin";
        data[0][1] = "adminone";
        data[0][2] = "root";
           ///   this will fail because the second vector of the matrix is taken
           ///   as the numbers parameters and the firs as the numbers of iterations to execute
           ///   to be passed to method so it does not match
           ///   if want to make this works uncomment the extra parameter in the m1d5 method and the extra output
           ///  to see the result and how it works and obviusly uncomment this  and comment the original one.
        data[1][0] = "123";
        data[1][1] = "321321";
        data[1][2] = "lolz";
*/
        return data;



    }
}
