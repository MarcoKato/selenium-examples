package ngTesting;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Day1 {

    @BeforeClass
    public void bfc() {
        System.out.println("im present before this class");
    }

    @AfterClass
    public void afc() {
        System.out.println("im present after this class");
    }

    @Parameters({"URL","USR","PSW"})
    @Test(groups = {"smoke"})
    public void m1d1(String URL, String USR, String PSW) {
        System.out.println("hallo testing!");
        System.out.println(URL);
        System.out.println(USR);
        System.out.println(PSW);
    }

    @Test(dependsOnMethods = {"m1d1"})
    public void m2d1() {
        System.out.println("hallo testing2!");
    }
}
