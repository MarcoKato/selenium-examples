package ngTesting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {

    @Test
    public void m0() throws IOException {

        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");

        Properties p = new Properties();
        FileInputStream fis = new FileInputStream("D:\\Documentos\\selenium-examples\\src\\main\\java\\ngTesting\\datadriver.properties");
        p.load(fis);
        WebDriver driver;
        switch (p.getProperty("browser")) {
            case "firefox":
                driver = new FirefoxDriver();
                break;
            default:
                driver = new ChromeDriver();
                break;
        }
        driver.get(p.getProperty("url"));
    }

    @Test
    public void m1() throws IOException {
        Properties p = new Properties();
        FileInputStream fis = new FileInputStream("D:\\Documentos\\selenium-examples\\src\\main\\java\\ngTesting\\datadriver.properties");
        p.load(fis);
        System.out.println(p.getProperty("username"));
        System.out.println(p.getProperty("password"));
        System.out.println(p.getProperty("url"));
    }
}
