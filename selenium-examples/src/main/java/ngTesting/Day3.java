package ngTesting;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Day3 {


    @BeforeTest
    public void bt() {
        System.out.println("im present when a complete test start");
    }

    @AfterTest
    public void at() {
        System.out.println("im present when a complete test finish");
    }

    @Test(groups = {"smoke"})
    public void m1d3() {
        System.out.println("web login");
    }

    @Test
    public void m2d3() {
        System.out.println("mobile login");
    }

    @Test
    public void m3d3() {
        System.out.println("api login");
    }
}
