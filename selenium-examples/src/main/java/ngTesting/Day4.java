package ngTesting;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Day4 {

    @BeforeSuite
    public void bs() {
        System.out.println("im in the beginning of the entire suite");
    }

    @AfterSuite
    public void as() {
        System.out.println("im in the finish of the entire suite");
    }

    @Test
    public void m1d4() {
        System.out.println("m1d4");
    }

    @Test
    public void m2d4() {
        System.out.println("m2d4");
    }

    @Test(groups = {"smoke"})
    public void m3d4() {
        System.out.println("m3d5");
    }
}
