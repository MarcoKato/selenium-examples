package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Alerts {
    public static void main(String[] args) throws InterruptedException {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////


        driver.get("http://rahulshettyacademy.com/AutomationPractice");

        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("maco");
        driver.findElement(By.xpath("//input[@id='alertbtn']")).click();
        Assert.assertTrue(driver.switchTo().alert().getText().contains("maco"));
        driver.switchTo().alert().accept();

        Thread.sleep(2000L);

        driver.findElement(By.xpath("//input[@id='name']")).sendKeys("maco");
        driver.findElement(By.xpath("//input[@id='confirmbtn']")).click();
        Assert.assertTrue(driver.switchTo().alert().getText().contains("maco"));
        //driver.switchTo().alert().accept();
        driver.switchTo().alert().dismiss();//it also can cancel or dismiss the alert
    }
}
