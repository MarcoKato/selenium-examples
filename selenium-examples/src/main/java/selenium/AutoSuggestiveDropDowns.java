package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoSuggestiveDropDowns {
    public static void main(String[] args) throws InterruptedException {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://makemytrip.com");
        driver.findElement(By.xpath("//span[contains(text(),'From')]")).click();
        driver.findElement(By.xpath("//input[@placeholder='From']")).sendKeys("a");
        String lookup = "Amritsar";
        while(true){
            driver.findElement(By.xpath("//input[@placeholder='From']")).sendKeys(Keys.ARROW_DOWN);
            Thread.sleep(500L);
            WebElement we = driver.findElement(By.xpath("//input[@placeholder='From']"));
            if(driver.findElement(By.xpath("//input[@placeholder='From']")).getText().equals(lookup)){
                driver.findElement(By.xpath("//input[@placeholder='From']")).sendKeys(Keys.ENTER);
                break;
            }
        }
    }
}
