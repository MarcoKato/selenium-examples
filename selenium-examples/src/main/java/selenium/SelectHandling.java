package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class SelectHandling {
    public static void main(String[] args) throws InterruptedException {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://spicejet.com");
        driver.findElement(By.xpath("//div[@id='divpaxinfo']")).click();
        Thread.sleep(1000L);
        //Select selenium object to handle selects
        Select s = new Select(driver.findElement(By.id("ctl00_mainContent_ddl_Adult")));
        s.selectByIndex(1);
        Thread.sleep(1000L);
        s.selectByValue("3");
        Thread.sleep(1000L);
        s.selectByVisibleText("2");

        System.out.println(driver.findElement(By.xpath("//div[@id='divpaxinfo']")).getText());
        //Assertions usualy used to validate and register fail or sucsess in test frameworks
        Assert.assertTrue(driver.findElement(By.xpath("//div[@id='divpaxinfo']")).getText().equals("2 Adult"));

    }
}
