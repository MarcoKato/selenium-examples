package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingAssertionPractice {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://rahulshettyacademy.com/seleniumPractise/#/offers");
        List<WebElement> fruits = driver.findElements(By.cssSelector("tr td:nth-child(2)"));
        ArrayList<String> fruitsAl = new ArrayList<>();

        System.out.println("*********************Original*************************");
        for (int i = 0; i < fruits.size() ; i++) {
            System.out.println(fruits.get(i).getText());
            fruitsAl.add(fruits.get(i).getText());
        }
        System.out.println("**********************copy************************");
        ArrayList<String> fruitsAlCopy = new ArrayList<>();
        for (int i = 0; i <fruitsAl.size() ; i++) {
            System.out.println(fruitsAl.get(i));
            fruitsAlCopy.add(fruitsAl.get(i));
        }
        System.out.println("***********************copy sorted***********************");
        Collections.sort(fruitsAlCopy);
        for (int i = 0; i <fruitsAlCopy.size() ; i++) {
            System.out.println(fruitsAlCopy.get(i));
        }
        driver.findElement(By.xpath("//b[contains(text(),'Veg/fruit name')]")).click();
        driver.findElement(By.xpath("//b[contains(text(),'Veg/fruit name')]")).click();
        System.out.println("***********************original sorted***********************");
        fruits = driver.findElements(By.cssSelector("tr td:nth-child(2)"));
        fruitsAl.clear();
        for (int i = 0; i < fruits.size() ; i++) {
            System.out.println(fruits.get(i).getText());
            fruitsAl.add(fruits.get(i).getText());
        }
        Assert.assertTrue(fruitsAlCopy.equals(fruitsAl));
    }
}
