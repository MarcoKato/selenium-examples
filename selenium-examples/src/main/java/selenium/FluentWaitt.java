package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.Function;


public class FluentWaitt {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://the-internet.herokuapp.com/dynamic_loading/1");
        driver.findElement(By.xpath("//button[contains(text(),'Start')]")).click();
        //h4[contains(text(),'Hello World!')]
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).
                withTimeout(Duration.ofSeconds(30)).
                pollingEvery(Duration.ofSeconds(3)).
                ignoring(NoSuchElementException.class);

        WebElement we = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                if (driver.findElement(By.cssSelector("[id='finish']")).isDisplayed())
                    return driver.findElement(By.cssSelector("[id='finish']"));
                else
                    return null;
            }
        });
        System.out.println(we.isDisplayed());
        System.out.println(we.getText());
    }
}
