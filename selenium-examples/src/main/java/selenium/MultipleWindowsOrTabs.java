package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.Set;

public class MultipleWindowsOrTabs {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("https://www.google.com/intl/es-419/gmail/about/");
        System.out.println(driver.getTitle());
        driver.findElement(By.linkText("Crea una cuenta")).click();
        System.out.println(driver.getTitle());
        Set<String> windowsOrTabs = driver.getWindowHandles();
        Iterator<String> it = windowsOrTabs.iterator();
        String parent = it.next();
        String child = it.next();
        driver.switchTo().window(child);
        System.out.println(driver.getTitle());
        driver.switchTo().window(parent);
        System.out.println(driver.getTitle());
    }
}
