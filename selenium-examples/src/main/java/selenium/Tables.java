package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Tables {
    public static void main(String[] args) throws InterruptedException {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("https://www.cricbuzz.com/");
        driver.findElement(By.xpath("//a[contains(text(),'Live Scores')]")).click();
        driver.findElement(By.xpath("//a[contains(text(),'AFG vs WI - WI Won')]")).click();
        driver.findElement(By.xpath("//a[contains(text(),'Scorecard')]")).click();
        Thread.sleep(2000L);
        WebElement we = driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr']"));
        //int rows  = we.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms']")).size();
        int count = we.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).size();
        int sum = 0;
        int result = 0;
        for (int i = 0; i < count -2; i++) {
            System.out.println(we.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).get(i).getText());
            sum += Integer.valueOf(we.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).get(i).getText());
        }
        System.out.println(we.findElement(By.xpath("//div[text()='Extras']/following-sibling::div")).getText());//to fin next sibling node :)
        sum += Integer.valueOf(we.findElement(By.xpath("//div[text()='Extras']/following-sibling::div")).getText());
        System.out.println("Expected= " + we.findElement(By.xpath("//div[text()='Total']/following-sibling::div")).getText());//to fin next sibling node :)
        result = Integer.valueOf(we.findElement(By.xpath("//div[text()='Total']/following-sibling::div")).getText());
        System.out.println("Result= " + sum);
        Assert.assertTrue(sum == result);//assure that te total is equal to the sum value

    }
}
