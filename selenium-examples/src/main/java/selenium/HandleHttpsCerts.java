package selenium;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HandleHttpsCerts {
    public static void main(String[] args) {

        DesiredCapabilities dc = DesiredCapabilities.chrome();
        //dc.acceptInsecureCerts();//accept insecure certificates
        dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

        ChromeOptions c = new ChromeOptions();
        c.merge(dc);//join the desired cap to the actual chrome options

        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        WebDriver driver = new ChromeDriver(c);
    }
}
