package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseAndAjax {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://www.amazon.com");

        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(By.cssSelector("a[id='nav-link-accountList']"))).build().perform();
        act.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).click().keyDown(Keys.SHIFT).sendKeys("hallo").build().perform();
        act.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).doubleClick().build().perform();
        act.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).contextClick().build().perform();
    }
}
