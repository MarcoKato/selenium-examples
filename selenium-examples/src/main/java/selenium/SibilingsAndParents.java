package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SibilingsAndParents {
    public static void main(String[] args) throws InterruptedException {


        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://qaclickacademy.com/interview");
        driver.findElement(By.xpath("//*[@id='tablist1-tab1']/following-sibling::li[2]")).click();//travel throug sibilings
        Thread.sleep(2000L);
        System.out.println(driver.findElement(By.xpath("//*[@id='tablist1-tab1']/parent::ul")).getText());//travel back to parent element that contains

    }
}
