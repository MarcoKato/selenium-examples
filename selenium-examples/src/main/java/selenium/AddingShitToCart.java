package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Arrays;
import java.util.List;

public class AddingShitToCart {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://rahulshettyacademy.com/seleniumPractise");

        String[] productsToAdd = {"Cucumber", "Brocolli", "Beetroot"};
        List itemsNeeded = Arrays.asList(productsToAdd);

        List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name"));
        String productName;

        for(int i = 0; i < products.size(); i++){
            productName = products.get(i).getText().split("-")[0].trim();
            if(itemsNeeded.contains(productName)){
                //driver.findElements(By.xpath("//button[text()='ADD TO CART']")).get(i).click();//problem due to text changing
                driver.findElements(By.xpath("//div[@class='product-action']//button")).get(i).click();//better way to identify
            }

        }
    }
}
