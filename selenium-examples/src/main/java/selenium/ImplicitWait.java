package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ImplicitWait {
    public static void main(String[] args) {
        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////

        driver.get("http://rahulshettyacademy.com/seleniumPractise");

        //Global Implicit wait!!!!!!!!!
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String[] productsToAdd = {"Cucumber", "Brocolli", "Beetroot"};
        List itemsNeeded = Arrays.asList(productsToAdd);

        List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name"));
        String productName;

        for(int i = 0; i < products.size(); i++){
            productName = products.get(i).getText().split("-")[0].trim();
            if(itemsNeeded.contains(productName)){
                //driver.findElements(By.xpath("//button[text()='ADD TO CART']")).get(i).click();//problem due to text changing
                driver.findElements(By.xpath("//div[@class='product-action']//button")).get(i).click();//better way to identify
            }

        }


        driver.findElement(By.xpath("//img[@alt='Cart']")).click();
        driver.findElement(By.xpath("//button[contains(text(),'PROCEED TO CHECKOUT')]")).click();
        driver.findElement(By.xpath("//input[@placeholder='Enter promo code']")).sendKeys("rahulshettyacademy");
        driver.findElement(By.xpath("//button[@class='promoBtn']")).click();
        //selenium has 10 seconds timeout to reach this element
        System.out.println(driver.findElement(By.xpath("//span[@class='promoInfo']")).getText());
        Assert.assertTrue(driver.findElement(By.xpath("//span[@class='promoInfo']")).getText().contains("successfully"));
    }
}
