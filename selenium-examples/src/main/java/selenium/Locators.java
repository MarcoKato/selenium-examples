package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Locators {
    public static void main(String[] args) {

        /////////Common///////////////////
        //Set driver property
        System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\selenium\\selenium-examples\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Documentos\\selenium\\selenium-examples\\geckodriver.exe");
        //Create driver object
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();
        /////////Common///////////////////


        //Facebook
        driver.get("http://facebook.com");

        //xpath
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys("hola");
        driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("adios");
        //driver.findElement(By.xpath("//input[@id='u_0_2']")).click(); //id's with numbers may vary so is not recomendable


        //id
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("pass")).clear();
        driver.findElement(By.id("email")).sendKeys("hola");
        driver.findElement(By.id("pass")).sendKeys("adios");
        //driver.findElement(By.id("u_0_2")).click();//id's with numbers may vary so is not recomendable

        //css
        driver.findElement(By.cssSelector("#email")).clear();
        driver.findElement(By.cssSelector("#pass")).clear();
        driver.findElement(By.cssSelector("#email")).sendKeys("hola");
        driver.findElement(By.cssSelector("#pass")).sendKeys("adios");
        //driver.findElement(By.xpath("//input[@type='submit']")).click();


        //linktext
        driver.findElement(By.linkText("¿Has olvidado los datos de la cuenta?")).click();
        //*[@id='login_form']/table/tbody/tr[3]/td[2]/div/a <<<<<< another way to find the same link


        driver.get("https://login.salesforce.com/?locale=mx");
        driver.findElement(By.id("username")).sendKeys("asd");
        driver.findElement(By.name("pw")).sendKeys("123");
        //driver.findElement(By.className("input r4 wide mb16 mt8 password")).click();//error cant provide separated clases, should be only 1 name class to be valid
        driver.findElement(By.xpath("//input[@value='Iniciar sesión']")).click();
        System.out.println(driver.findElement(By.id("error")).getText());//get the inner text of the element

        /*
            Evey object may not have ID, className or name- Xpath and CSS Preferred
            Alpha numeric id may vary on every refresh- check
            Confirm the link object with anchor "a" tag
            Classes should not have spaces- Compound classes cannot be accepted
            Multipl values - Selenium identifies the first one- Scans from top left
            Double quotes inside double quotes are not accepted
            Xpath/CSS  can be defined in n number of ways
            Rightclick copy on blue highlighted html code to generate xpath
            Firepath depreciated from firefox-
            when xpath starts with html-Not reliable- Switch browser to get another one
            There is no direct way to get CSS in chrome. You will find it in tool bar
            Degrade browser to less firefox 55 to ge Firepath
            $("") - for css ,    $x("")   or xpath
            //tagName[@attribute='value']  - xpath syntax
            tagName[attribute='value']  -CSS  tagName#id- CSS   tagname.classname- CSS
            //tagName[contains(@attribute,'value')]  - xpath regular expression
            tagName[Atrribute*='value'] - Css regular expression
         */

    }
}
