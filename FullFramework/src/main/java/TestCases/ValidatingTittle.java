package TestCases;

import Academy.base;
import PageObjects.LandingPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class ValidatingTittle extends base {

    public static Logger log = LogManager.getLogger(base.class.getName());

    @BeforeTest
    public void initialize() throws IOException {
        driver = InitializeDriver();
        log.info("Driver is Initialized");

        driver.get(prop.getProperty("url"));
        log.info("Navigated to webpage");
    }

    @Test
    public void basePageNavigation() {
        //driver = InitializeDriver();
        //driver.get("http://qaclickacademy.com");
        //driver.get(prop.getProperty("url"));
        LandingPage lp = new LandingPage(driver);
        System.out.println(lp.getMiddleTittle().getText());
        Assert.assertEquals(lp.getMiddleTittle().getText().toLowerCase(),"Featured Courses".toLowerCase());
    }

    @Test
    public void validatingHeader(){
        LandingPage lp = new LandingPage(driver);
        System.out.println(lp.getHeader().getText());
        Assert.assertEquals(lp.getHeader().getText().toLowerCase(),"An Academy to learn Everything about Testing".toLowerCase());
    }
    @AfterTest
    public void kill(){
        driver.close();
        driver = null;
    }
}
