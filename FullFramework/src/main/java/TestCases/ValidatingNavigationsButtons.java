package TestCases;

import Academy.base;
import PageObjects.LandingPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class ValidatingNavigationsButtons extends base {

    public static Logger log = LogManager.getLogger(base.class.getName());

    @BeforeTest
    public void initialize() throws IOException {
        driver = InitializeDriver();
        log.info("Driver is Initialized");

        driver.get(prop.getProperty("url"));
        log.info("Navigated to webpage");
    }

    @Test
    public void basePageNavigation() {
        //driver = InitializeDriver();
        //driver.get("http://qaclickacademy.com");
        //driver.get(prop.getProperty("url"));
        LandingPage lp = new LandingPage(driver);
        System.out.println(lp.getMiddleTittle().getText());
        for (WebElement we : lp.getnavigationButtons()) {
            Assert.assertTrue(we.isDisplayed());
            System.out.println(we.getText());
        }
    }

    @AfterTest
    public void kill(){
        driver.close();
        driver = null;
    }


}
