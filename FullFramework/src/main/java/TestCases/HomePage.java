package TestCases;

import Academy.base;
import PageObjects.LandingPage;
import PageObjects.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class HomePage extends base {

    public static Logger log = LogManager.getLogger(base.class.getName());

    @BeforeTest
    public void initialize() throws IOException {
        driver = InitializeDriver();
        log.info("Driver is Initialized");

        driver.get(prop.getProperty("url"));
        log.info("Navigated to webpage");
    }

    @Test(dataProvider = "getData")
    public void basePageNavigation(String usr, String psw) {
        //driver = InitializeDriver();
        //driver.get("http://qaclickacademy.com");
        LandingPage lp = new LandingPage(driver);
        lp.getLoginButton().click();
        LoginPage logp = new LoginPage(driver);
        logp.getUsernameField().sendKeys(usr);
        logp.getPasswordField().sendKeys(psw);
        logp.getSubmitButton().click();
        log.info(helper);
        Assert.assertEquals(lp.getError().getText(),"Invalid email or password.");
        log.info("success");
    }
    @AfterTest
    public void kill(){
        driver.close();
        driver = null;
    }

    @DataProvider
    public Object[][] getData(){
        Object[][] data = new Object[1][2];
        data[0][0] = "admin@spm.com";
        data[0][1] = "pass12332543";
        return data;
    }

}
