package Academy;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class base {

    protected static WebDriver driver;
    protected static String helper;
    protected Properties prop;

    public WebDriver InitializeDriver() throws IOException {

        //Load data properties
        helper = System.getProperty("user.dir");
        prop = new Properties();
        FileInputStream fis = new FileInputStream(helper+"\\src\\main\\java\\res\\data.properties");
        prop.load(fis);

        String s = System.getProperty("browser");
        //Set system property for web drivers
        System.setProperty("webdriver.chrome.driver", helper+"\\src\\main\\resources\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", helper+"\\src\\main\\resources\\geckodriver.exe");
        //
        ChromeOptions op = null;
        FirefoxOptions opfx = null;

        if(System.getProperty("hided").equals("yes")){
            op = new ChromeOptions();
            opfx = new FirefoxOptions();
            op.addArguments("headless");
            opfx.addArguments("headless");
        }

        //Select browser
        switch (s/*prop.getProperty("browser")*/){
            case "firefox":
                driver = opfx != null ? new FirefoxDriver(opfx) : new FirefoxDriver();
                break;
            default:
                driver =  op != null ? new ChromeDriver(op) : new ChromeDriver();
                break;
        }
        //Set implicit timeout
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }
    public Properties getProp() {prop= new Properties();
        return prop;
    }

    public static void getScreenShot(String name) throws IOException {
        File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(helper + "\\outputs\\"+name+".png"));
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
