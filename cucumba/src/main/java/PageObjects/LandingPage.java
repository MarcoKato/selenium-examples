package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class LandingPage {
    private WebDriver driver;

    private By loginButton = By.cssSelector("a[href*='sign_in']");
    private By middleTittle = By.xpath("//h2[contains(text(),'Featured Courses')]");
    private By navigationButtons = By.xpath("//ul[@class='nav navbar-nav navbar-right']//a");
    private By error = By.xpath("//div[@class='alert alert-danger']");
    private By header = By.xpath("//div[@class='carousel-caption']//div//h3");

    public WebElement getError() {
        return driver.findElement(error);
    }

    public LandingPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getLoginButton() {
        return driver.findElement(loginButton);
    }

    public WebElement getMiddleTittle() {
        return driver.findElement(middleTittle);
    }

    public WebElement getHeader() {
        return driver.findElement(header);
    }

    public List<WebElement> getnavigationButtons(){
        return driver.findElements(navigationButtons);
    }
}
