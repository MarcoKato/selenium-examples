package cucumber.options;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

//Feature
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/main/java/Features",
        glue = "StepDefinitions")
public class TestRunner {

}
