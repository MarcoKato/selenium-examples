package StepDefinitions;

import Academy.base;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
public class stepDefinition extends base {

    @Given("^User is on NetBanking landing page$")
    public void user_is_on_netbanking_landing_page() {
        System.out.println("user is on netbanking landing page");
    }

    @When("^User login into application with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void user_login_into_application_with_something_and_something(String usr, String pass) {
        System.out.println("user login into application with");
        System.out.println(usr);
        System.out.println(pass);
    }

    @Then("^Home page is displayed$")
    public void home_page_is_displayed() {
        System.out.println("home page is displayed");
    }

    @And("^Cards are displayed$")
    public void cards_are_displayed() {
        System.out.println("cards are displayed");

    }
}
