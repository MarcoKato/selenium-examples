package StepDefinitions;

import Academy.base;
import PageObjects.LandingPage;
import PageObjects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import org.testng.Assert;


import java.io.IOException;
import java.util.Optional;

@RunWith(Cucumber.class)
public class loginDefinition extends base {

    @Given("^initialize the browser with \"([^\"]*)\"$")
    public void initialize_the_browser_with_something(String strArg1) throws IOException {
        driver = InitializeDriver(Optional.of(strArg1));
    }

    @When("^User type the \"([^\"]*)\" and \"([^\"]*)\" and submit$")
    public void user_type_the_something_and_something_and_submit(String strArg1, String strArg2){
        LoginPage lp = new LoginPage(driver);
        lp.getUsernameField().sendKeys(strArg1);
        lp.getPasswordField().sendKeys(strArg2);
        lp.getSubmitButton().click();
    }

    @Then("^Verify that the user successfully logged in$")
    public void verify_that_the_user_successfully_logged_in(){
        LoginPage lp = new LoginPage(driver);
        Assert.assertTrue(lp.getError().isDisplayed());//should be false :v
    }

    @And("^Navigate to \"([^\"]*)\" site$")
    public void navigate_to_something_site(String strArg1){
        driver.get(strArg1);
    }

    @And("^Click on login link in home page to land on secure login page$")
    public void click_on_login_link_in_home_page_to_land_on_secure_login_page(){
        LandingPage lp = new LandingPage(driver);
        lp.getLoginButton().click();
    }
}
